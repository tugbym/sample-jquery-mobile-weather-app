$( document ).ready(function() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        $( '#weather' ).append("Geolocation is not supported by this browser.");
    }
});

$( document ).bind( "mobileinit", function() {
    // Make your jQuery Mobile framework configuration changes here!

    $.mobile.allowCrossDomainPages = true;
    $.support.cors = true;
});
                    
function showPosition(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    $.ajax({
       url:'http://api.openweathermap.org/data/2.5/forecast/daily?lat=' + latitude + '&lon=' + longitude + '&units=metric&mode=json&cnt=7',
       success: function(result) {
           $( '#location' ).append("<b>" + result.city.name + ", " + result.city.country + "</b>");
           for (var i=0; i<result.list.length; i++) {
               $( 'tbody' ).append("<tr><td>" + timeConverter(result.list[i].dt) + "</td><td>" + result.list[i].weather[0].description + "</td><td>" + "<img src='http://openweathermap.org/img/w/" + 
                                   result.list[i].weather[0].icon + ".png' alt='icon'>" + "</td></tr>");
           }
       }
    });
}
        
function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp*1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var formattedDate = date + ' ' + month + ' ' + year;
  return formattedDate;
}